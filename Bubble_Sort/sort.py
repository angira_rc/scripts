def sort(arr, order):
	while True:
		done = False

		if order == 'D':
			for i in range(0, len(arr) - 1):
				if arr[i] > arr[i + 1]:
					arr[i], arr[i + 1] = arr[i + 1], arr[i]
					done = True

		elif order == 'A':
			for i in range(1, len(arr) - 1):
				if arr[i] < arr[i - 1]:
                                        arr[i], arr[i + 1] = arr[i + 1], arr[i]
                                        done = True

		else:
			return 'Order type should be either A/D'
		
		if not done:
			return arr
