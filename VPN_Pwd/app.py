__author__ = 'rei'

import requests, re
from urllib import request

html = requests.get('https://www.vpnbook.com/').content
pattern = re.compile(r'password.php\?t=[\d|.\s]+')
link = pattern.search(str(html)).group()
img = request.urlretrieve(f'https://www.vpnbook.com/{link}', 'pwd.png')
